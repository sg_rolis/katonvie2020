var CART_IS_VISIBLE = false;
$(document).ready(function(){
	var cart = $('#cart-popup-list');
	var overlay = $('#cart-popup-overlay');
	var items = cart.find('.cart-popup-items');
	var itemCount = cart.find('[data-cart-popup-cart-quantity]');
	var itemCountBubble = $('[data-cart-count-bubble]');
	var total = cart.find('.cart-popup-total h5');
	var checkout = cart.find('.checkout');
	
	checkout.click(function(){
		window.location.href = "/checkout";
	});
	
	function update(){
		items.children('.cart-popup-item').not(':first').remove();
		items.find('[data-loader]').removeClass('hide');
		
		$.post({
			url: '/cart.js',
			dataType: 'json'
		}).done(function(response){
			items.find('[data-loader]').addClass('hide');
			var template = cart.find('.cart-popup-item');
			
			itemCount.html(response.item_count);
			itemCountBubble.html(response.item_count);
			total.html(toCurrency(response.total_price)).attr('data-total', response.total_price);
			
			$.each(response.items, function(index){
				var item = template.clone();
				item.attr({
					'data-id': this.variant_id,
					'data-line': index + 1
				});
				item.find('img').attr('src', this.featured_image.url);
				item.find('.cart-popup-item__title').html(this.product_title);
				item.find('.cart-popup-item__collection').html('&nbsp;');
				item.find('.cart-popup-item__price').html(toCurrency(this.final_price)).attr('data-price', this.final_price);
				item.find('input[type="number"]').val(this.quantity).attr('data-qty', this.quantity);
				item.removeClass('hide');
				items.append(item);
			});
			
			parse();
		}).fail(function(){
			
		});
	}
	
	function show(){
		CART_IS_VISIBLE = true;
		if(!$('.product-popup-wrapper').hasClass('product-popup-wrapper-hidden')){
			$('.product-popup-wrapper').prepareTransition().addClass('product-popup-wrapper-hidden');
		}
		$('#cart-popup-just-added').find('[data-cart-popup-close]').click();
		cart.prepareTransition().removeClass('cart-popup-wrapper--hidden');
		cart.find('[data-cart-popup-close]').click(hide);
		$('body').click(hide);
		overlay.removeClass('hidden');
		update();
	}
	function hide(){
		CART_IS_VISIBLE = false;
		cart.prepareTransition().addClass('cart-popup-wrapper--hidden');
		cart.find('[data-cart-popup-close]').unbind('click', hide);
		$('body').unbind('click', hide);
		overlay.addClass('hidden');
	}
	
	$('.site-header__cart').click(function(event){
		event.preventDefault();
		event.stopPropagation();
		
		if(cart.hasClass('cart-popup-wrapper--hidden')){
			show();
		}
		else{
			hide();
		}
	});
	
	cart.click(function(event){
		event.stopPropagation();
	});
	
	$('#cart-popup-just-added .cart-popup__view a').click(function(event){
		event.preventDefault();
		event.stopPropagation();
		$('.site-header__cart').click();
	});
	
	function parse(){
		cart.find('input[type="number"]').unbind('change', qtyOnChange).change(qtyOnChange);
		cart.find('input[type="number"]').parent().children('span').unbind('click', updateQty).click(updateQty);
		cart.find('.cart-popup-item-remove').unbind('click', remove).click(remove);
		checkButtons();
	}
	function updateQty(){
		var parent = $(this).closest('.cart-popup-item');
		var input = parent.find('input[type="number"]');
		var qty = parseInt(input.val());
		if($(this).hasClass('plus')){
			qty++;
		}
		else if($(this).hasClass('minus')){
			qty--;
		}
		input.val(qty > 0 ? qty : 1).change();
	}
	var noZero = true;
	function qtyOnChange(){
		var parent = $(this).closest('.cart-popup-item');
		var input = parent.find('input[type="number"]');
		var min = noZero ? 1 : 0;
		
		var before = parseInt(input.attr('data-qty'));
		var after = parseInt(input.val());
		if(after < min){
			input.val(min);
			after = min;
		}
		input.attr('data-qty', after);
		
		itemCount.html(parseInt(itemCount.html()) + (after - before));
		itemCountBubble.html(itemCount.html());
		
		$.post({
			url: '/cart/change.js',
			data: {
				quantity: after,
				line: parent.attr('data-line')
			},
			dataType: 'json'
		}).done(function(){
			
		}).fail(function(){
			
		});
		
		var price = parseFloat(parent.find('.cart-popup-item__price').attr('data-price'));
		var newTotal = parseFloat(total.attr('data-total'));
		newTotal += ((after - before) * price);
		total.attr('data-total', newTotal).html(toCurrency(newTotal));
	}
	function remove(){
		var parent = $(this).closest('.cart-popup-item');
		noZero = false;
		parent.find('input[type="number"]').val(0).change();
		noZero = true;
		parent.remove();
		checkButtons();
	}
	
	function checkButtons(){
		if(cart.find('.cart-popup-item').length == 1){
			checkout.hide();
		}
		else{
			checkout.show();
		}
	}
	
	function toCurrency(price){
		var price = (price / 100).toString();
		var priceTxt = '';
		var pos = price.length;
		var count = 0;
		while(pos >= 0){
			if(count > 3){
				priceTxt = '.' + priceTxt;
				count = 1;
			}
			priceTxt = price.charAt(pos) + priceTxt;
			count++;
			pos--;
		}
		return priceTxt + ',00';
	}
});